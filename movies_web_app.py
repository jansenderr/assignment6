import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None)
    username = os.environ.get("USER", None)
    password = os.environ.get("PASSWORD", None)
    hostname = os.environ.get("HOST", None)
    #db = 'movies'
    #username = 'root'
    #password = 'password'
    #hostname = 'localhost'
    return db, username, password, hostname


def create_table():
    print('herebitch')
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movie(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year YEAR(4), title TEXT, director TEXT, actor TEXT, release_date DATE, rating FLOAT, PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
        print('done')
    except Exception as exp:
        print('exceptionalright')
        print(exp)
        import MySQLdb
        #try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def populate_data():

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                       host=hostname,
                                       database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()
    cur.execute("INSERT INTO movie (year, title, director, actor, release_date, rating) values (2002," 
        "'Spirited Away', 'Hayao Miyazaki', 'Daveigh Chase', '2002-09-20', 9.7)")
    cur.execute("INSERT INTO movie (year, title, director, actor, release_date, rating) values (1993,"
        "'My Neighbor Totoro', 'Hayao Miyazaki', 'Chika Sakamoto', '1993-05-07', 9.4)")
    cur.execute("INSERT INTO movie (year, title, director, actor, release_date, rating) values (2008,"
        "'Funny Games', 'Michael Haneke', 'Naomi Watts', '2008-03-14', 5.2)")
    cur.execute("INSERT INTO movie (year, title, director, actor, release_date, rating) values (1985,"
        "'The Breakfast Club', 'John Hughes', 'Emilio Estevez', '1985-02-15', 8.8)")
    cnx.commit()
    print("Returning from populate_data")


def query_data():

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        #try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    cur.execute("SELECT title FROM movie")
    entries = cur.fetchall()
    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table")
    create_table()
    print("After create_table")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/search_movie', methods=['GET'])
def search_movie():
    print("Received request.")
    print(request.args.get('search_actor'))
    msg = request.args.get('search_actor')
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()
    message = ''
    cur.execute("SELECT title, year, actor FROM movie WHERE actor = \'{0}\'".format(msg))
    try:
        entries = cur.fetchall()
        if len(entries) == 0:
            message = 'No movies found for actor \'{0}\''.format(msg)
        else:
            for row in entries:
                message = message + '{0}, {1}, {2} \n'.format(row[0],row[1],row[2])
    except Exception as exp:
        message = 'No movies found for actor \'{0}\''.format(msg)
    return hello(message)

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    print("Received request.")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()
    message = ''
    cur.execute("SELECT title, year, actor, director, rating FROM movie ORDER BY rating DESC")
    try:
        entries = cur.fetchall()
        if len(entries) == 0:
            message = 'No movies in database'
        else:
            highest = entries[0][4]
            for row in entries:
                if(row[4] == highest):
                    message = message + '{0}, {1}, {2}, {3}, {4} \n'.format(row[0],row[1],row[2],row[3],row[4])
                else:
                    break
    except Exception as exp:
        print(exp);
        message = 'No movies in database'
    return hello(message)

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    print("Received request.")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()
    message = ''
    cur.execute("SELECT title, year, actor, director, rating FROM movie ORDER BY rating ASC")
    try:
        entries = cur.fetchall()
        if len(entries) == 0:
            message = 'No movies in database'
        else:
            lowest = entries[0][4]
            for row in entries:
                if(row[4] == lowest):
                    message = message + '{0}, {1}, {2}, {3}, {4} \n'.format(row[0],row[1],row[2],row[3],row[4])
                else:
                    break
    except Exception as exp:
        print(exp);
        message = 'No movies in database'
    return hello(message)

@app.route('/add_movie', methods=['POST'])
def add_movie():
    print("Received request.")
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    message = ''
    cur = cnx.cursor()
    cur.execute("SELECT * FROM movie WHERE title = \'{0}\'".format(title))
    entries = cur.fetchall()
    if len(entries) > 0:
        message = 'Movie with title \"{0}\" already exists'.format(title)
    else:
        try:
            cur.execute("INSERT INTO movie (year,title,director,actor,release_date,rating) values "
            "(\'{0}\',\'{1}\',\'{2}\',\'{3}\',\'{4}\',\'{5}\')".format(year,title,director,actor,release_date,rating))
            cnx.commit()
            message = 'Movie \'{0}\' successfully inserted'.format(title)
        except Exception as exp:
            print(exp)
            message = 'Movie \"{0}\" could not be created - {1}'.format(title,str(exp))
    return hello(message)

@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received request.")
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    message = ''
    cur = cnx.cursor()
    cur.execute("SELECT * FROM movie WHERE title = \'{0}\'".format(title))
    entries = cur.fetchall()

    if len(entries) == 0:
        message = 'Movie with title \"{0}\" does not exist'.format(title)
    else:
        cur.execute("UPDATE movie SET year = \'{0}\', title = \'{1}\', director = \'{2}\',actor = \'{3}\',release_date = \'{4}\', rating = \'{5}\'"
        "WHERE title = \'{6}\'".format(year,title,director,actor,release_date,rating,title))
        try:
            cnx.commit()
            message = 'Movie \'{0}\' successfully updated'.format(title)
        except Exception as exp:
            message = 'Movie \"{0}\" could not be updated - {1}'.format(title,str(exp))
    return hello(message)

@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    print("Received request.")
    msg = request.form['delete_title']

    db, username, password, hostname = get_db_creds()

    print("Inside delete_movie")

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()
    cur.execute("SELECT * FROM movie WHERE title = \'{0}\'".format(msg))
    entries = cur.fetchall()
    message = ''

    if len(entries) == 0:
        message = 'Movie with title \"{0}\" does not exist'.format(msg)
    else:
        try:
            cur.execute('DELETE FROM movie WHERE title = \'{0}\''.format(msg))
            cnx.commit()
            message = 'Movie \"{0}\" successfully deleted'.format(msg)
        except Exception as exp:
            message = 'Movie \"{0}\" could not be deleted - {1}'.format(msg,str(exp))

    return hello(message)

@app.route("/")
def hello(message = ''):
    print("Before displaying index.html")
    entries = query_data()
    print("Entries: %s" % entries)
    return render_template('index.html', entries=entries, message=message)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
